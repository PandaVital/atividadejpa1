package br.ucsal.bd2.atividadeJpa1;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_funcionario", uniqueConstraints = {
		@UniqueConstraint(name = "un_funcionario_cpf", columnNames = { "cpf" }) })

public class Funcionario {
	@Id
	// String cpf - char(11) - not null
	private Integer cpf;

	@Column(unique = true)
	// Integer matricula - integer - not null - chave prim�ria
	private Integer matricula;

	// String nome - varchar(40) - not null
	private String nome;
	

	// Date dataAdmissao - date - not null
	Date dataAdmissao;

	// Date dataDemissao - date - null
	Date dataDemissao;

	@Embedded
	// Departamento departamento - not null
	Departamento departamento = new Departamento();

	@Embedded
	// TipoFuncionarioEnum tipo- char(3) - not null
	TipoFuncionario tipoFuncionario;

	public Funcionario(Integer matricula, String nome, Integer cpf, Departamento departamento, Date dataAdmissao,
			Date dataDemissao, TipoFuncionario tipoFuncionario) {
		super();
		this.matricula = matricula;
		this.nome = nome;
		this.cpf = cpf;
		this.departamento = departamento;
		this.dataAdmissao = dataAdmissao;
		this.dataDemissao = dataDemissao;
		this.tipoFuncionario = tipoFuncionario;
	}

	public Funcionario() {
		super();
	}

	public Integer getMatricula() {
		return matricula;
	}

	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCpf() {
		return cpf;
	}

	public void setCpf(Integer cpf) {
		this.cpf = cpf;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Date getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(Date dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public Date getDataDemissao() {
		return dataDemissao;
	}

	public void setDataDemissao(Date dataDemissao) {
		this.dataDemissao = dataDemissao;
	}

	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}

	public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}

	@Override
	public String toString() {
		return "Funcionario [matricula=" + matricula + ", nome=" + nome + ", cpf=" + cpf + ", departamento="
				+ departamento + ", dataAdmissao=" + dataAdmissao + ", dataDemissao=" + dataDemissao
				+ ", tipoFuncionario=" + tipoFuncionario + "]";
	}

}
