package br.ucsal.bd2.atividadeJpa1;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_departamento", uniqueConstraints = {
		@UniqueConstraint(name = "un_departamento_codigo", columnNames = { "codigo" }) })
public class Departamento {
	
	@Id
	// String codigo - char(5) - not null - chave prim�ria
	private String codigo;
	
	@Column(unique = true)
	// String nome - varchar(40) - not null
	private String nome;
	
	@ManyToMany
	// List<Funcionario> funcionarios
	private List<Funcionario> funcionario;

	@Override
	public String toString() {
		return "Departamento [codigo=" + codigo + ", nome=" + nome + ", funcionario=" + funcionario + "]";
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Funcionario> getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(List<Funcionario> funcionario) {
		this.funcionario = funcionario;
	}

	public Departamento(String codigo, String nome, List<Funcionario> funcionario) {
		super();
		this.codigo = codigo;
		this.nome = nome;
		this.funcionario = funcionario;
	}

	public Departamento() {
		super();
	}
	
}
