package br.ucsal.bd2.atividadeJpa1;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("atividadejpa1");
		
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();
		
		Departamento departamento1 = new Departamento("12", "Jambo", null);
		
		Funcionario funcionario1 =new Funcionario(123456789, "Roberto", 1, departamento1, null, null, TipoFuncionario.FXO);
		
		Funcionario funcionario2 =new Funcionario(123455874, "guilherme", 2, departamento1, null, null, TipoFuncionario.FXO);

		
		em.persist(departamento1);
		
		em.persist(funcionario1);
		
		em.persist(funcionario2);

		em.getTransaction().commit();

		
}
}
